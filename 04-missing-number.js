const assert = require('assert');

// Good morning! Here's your coding interview problem for today.

// This problem was asked by Stripe.

// Given an array of integers, find the first missing positive integer in linear time and constant space. 
// In other words, find the lowest positive integer that does not exist in the array. The array can contain duplicates and negative numbers as well.

// For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.

// You can modify the input array in-place.

const input1 = [3, 4, -1, 1];
const output1 = 2;

const input2 = [1, 2, 0];
const output2 = 3; // !!!

const findSmallestPositiveMissingInt =
(
    arr
) => {
    const map = {};
    let biggestNumber = 0;
    for (let val of arr) {
        map[val] = true;
        if (val > biggestNumber) biggestNumber = val;
    }
    
    let current = 0;

    do {
        current++;
        if (!map[current]) {
            return current;
        }        
    } while(current <= biggestNumber);    

    return current;
}

const result1 = findSmallestPositiveMissingInt(input1);
assert.strictEqual(result1, output1);

const result2 = findSmallestPositiveMissingInt(input2);
assert.strictEqual(result2, output2);

