const assert = require('assert');

// Good morning! Here's your coding interview problem for today.

// This problem was asked by Jane Street.

// cons(a, b) constructs a pair, 
// and car(pair) and cdr(pair) returns the first and last 
// element of that pair. 
// For example, car(cons(3, 4)) returns 3, 
// and cdr(cons(3, 4)) returns 4.

// Given this implementation of cons:

// def cons(a, b):
//     def pair(f):
//         return f(a, b)
//     return pair
// Implement car and cdr.

// the declaration above translated to ts would be

function cons(a, b) {
    function pair(f) {
        return f(a, b);
    }
    return pair;
}

function car(pair) {
    function _car(a, b) {
        return a;
    }
    return pair(_car); 
}

function cdr(pair) {
    function _cdr(a, b) {
        return b;
    }
    return pair(_cdr); 
}

assert.strictEqual(car(cons(3, 4)), 3);
assert.strictEqual(cdr(cons(3, 4)), 4);

console.log('done');
