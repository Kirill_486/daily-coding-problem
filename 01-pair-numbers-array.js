// Good morning! Here's your coding interview problem for today.

// This problem was recently asked by Google.

// Given a list of numbers and a number k, return whether any two numbers from the list add up to k.

// For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.

// Bonus: Can you do this in one pass?

const numbersAdd =
(
    array,
    k
) => {
    for (let el1 of array) {
        for (let el2 of array) {
            if ((el1 + el2) === k) return true;
        }
    }
    return false;
}

const numbersAdd2 = (
    array,
    argument
) => {
    const halfs = {};
    for (let element of array) {
        if (halfs[element]) return true;
        halfs[argument - element] = true;        
    }
    return false;
}

const input1 = [10, 15, 3, 7];
const k1 = 17;

console.log(numbersAdd(input1, k1));
console.log(numbersAdd2(input1, k1));
console.log(numbersAdd([10, 15, 3, 2], k1));
console.log(numbersAdd2([10, 15, 3, 2], k1));
console.log(numbersAdd([10, 15, 3, 1], k1));
console.log(numbersAdd2([10, 15, 3, 1], k1));
