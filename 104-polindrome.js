// Good morning! Here's your coding interview problem for today.

// This problem was asked by Google.

// Determine whether a doubly linked list is a palindrome. What if it’s singly linked?

// For example, 1 -> 4 -> 3 -> 4 -> 1 returns True while 1 -> 4 returns False

// 1 3 6 8 12 8 6 3

const assert = require('assert');

class DoubleLinkedNode {
    
    constructor(
        value,
    ) {
        this.value = value;
        this.previosNode = null;
        this.nextNode = null;  
    }

    setPreviousNode(node) {
        this.previosNode = node;
    }

    setNextNode(node) {
        this.nextNode = node;
    }
}

class SingleLinkedNode {
    
    constructor(
        value,
    ) {
        this.value = value;
        this.nextNode = null;  
    }

    setPreviousNode(node) {
        throw new Error('Not implemented')    
    }

    setNextNode(node) {
        this.nextNode = node;
    }
}

class DoubliLinkedList {
    constructor(nodeValues) {
        const nodes = [];

        this.length = 0;

        for (let node of nodeValues) {
            const listNode = new DoubleLinkedNode(node);
            nodes.push(listNode);
            this.length++;
        }

        const nodesLength = nodes.length;

        nodes.forEach((node, index) => {
            const ifFirstNode = index === 0;
            const isLastNode = index === nodesLength - 1;

            if (!ifFirstNode) {
                node.setPreviousNode(nodes[index - 1]);
            } else {
                this.firstNode = node;
            }

            if (!isLastNode) {
                node.setNextNode(nodes[index + 1]);
            } else {
                this.lastNode = node;
            }
        });
    }
    
    isPolindrome() {
        let currentHead = this.firstNode;
        let currentTail = this.lastNode;
        let currentIndex = 0;
        let maxIndex = Math.ceil(this.length / 2);

        let isPolindrome = true;

        while (currentIndex < maxIndex) {
            isPolindrome = currentHead.value === currentTail.value;
            currentHead = currentHead.nextNode;
            currentTail = currentTail.previosNode;

            currentIndex++;

            if (!isPolindrome) {
                break;
            }
        }
        return isPolindrome;
    }
}

class SingleLinkedList {
    constructor(nodeValues) {
        this.length = 0;
        this.nodes = [];

        for (let node of nodeValues) {
            const listNode = new SingleLinkedNode(node);
            this.nodes.push(listNode);
            this.length++;
        }

        const nodesLength = this.nodes.length;

        this.nodes.forEach((node, index) => {
            const ifFirstNode = index === 0;
            const isLastNode = index === nodesLength - 1;

            if (ifFirstNode) {
                this.firstNode = node;
            }

            if (!isLastNode) {
                node.setNextNode(this.nodes[index + 1]);
            } else {
                this.lastNode = node;
            }
        });
    }
    
    isPolindrome() {
        let currentHead = this.firstNode;
        let maxIndex = Math.floor(this.length / 2);

        let hashFront = 0;
        let hashBack = 0

        for (let i = 0; i < maxIndex; i++) {
            const value = currentHead.value;
            // console.log(`front - ${value}`);
            hashFront += ((i+1) * 100) * value;
            currentHead = currentHead.nextNode;
        }

        const isLengthUneven = !!(this.length % 2);
        if (isLengthUneven) {
            // console.log(`uneven - ${currentHead.value}`);
            currentHead = currentHead.nextNode;

        }

        for (let i = maxIndex-1; i >= 0; i--) {
            const value = currentHead.value;
            // console.log(`back - ${value}`);
            hashBack += ((i+1) * 100) * value;
            currentHead = currentHead.nextNode;
        }

        // console.log(`isPolindrome ${hashFront} = ${hashBack}`);

        return hashFront === hashBack;
    }
}

let isPolindrome;

const polindrome = [1, 4, 3, 4, 1];

const doubleLinkedListPolindrome = new DoubliLinkedList(polindrome);
isPolindrome = doubleLinkedListPolindrome.isPolindrome();
assert.equal(isPolindrome, true);

const singleLinkedListPolindrome = new SingleLinkedList(polindrome);
isPolindrome = singleLinkedListPolindrome.isPolindrome();
assert.equal(isPolindrome, true);

const notPolindrome = [1, 2, 3];

const doubleLinkedListNotPolindrome = new DoubliLinkedList(notPolindrome);
isPolindrome = doubleLinkedListNotPolindrome.isPolindrome();
assert.equal(isPolindrome, false);

const singleLinkedListNotPolindrome = new SingleLinkedList(notPolindrome);
isPolindrome = singleLinkedListNotPolindrome.isPolindrome();
assert.equal(isPolindrome, false);

const biggerPolindrome = [1, 3, 6, 8, 12, 8, 6, 3, 1];

const doubleLinkedListBiggerPolindrome = new DoubliLinkedList(biggerPolindrome);
isPolindrome = doubleLinkedListBiggerPolindrome.isPolindrome();
assert.equal(isPolindrome, true);

const singleLinkedListBiggerPolindrome = new SingleLinkedList(biggerPolindrome);
isPolindrome = singleLinkedListBiggerPolindrome.isPolindrome();
assert.equal(isPolindrome, true);